#!/usr/bin/env python3


from iw import get_parsed_cells
import subprocess
import RPi.GPIO as GPIO
from time import sleep
import json
import pysher
import configuration
import requests
import git
import sys
import os
import threading
import datetime


version = '2.1.0'

is_registered = False

print('Starting... ', version)


def getSerial():
    cpuserial = None
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        print('error getting serial')

    if cpuserial:
        try:
            r = requests.post(configuration.host + '/api/crosses/register', data={'serial': cpuserial, 'version': version}, headers={
                              'Authorization': configuration.passphrase, 'Accept': 'application/json'})
            if r.status_code == 200:
                is_registered = True
            # print(r.status_code)

        except:
            print('cannot connect to register api')

    return cpuserial


def getTemp():
    cputemp = None
    try:
        f = open("/sys/class/thermal/thermal_zone0/temp", "r")
        t = float(f.readline()) / 1000
        cputemp = str(round(t, 1))
        f.close()
    except:
        print('error getting temp')

    return cputemp


def getSignalStrength():
    ssid = None
    strength = None
    mac = None
    result = subprocess.run(['iw', 'wlan0', 'link'], stdout=subprocess.PIPE)
    output = result.stdout.decode('utf-8').split('\n')

    for line in output:
        split = line.replace('Connected to ', '').replace(
            ' (on wlan0)', '').split(':')
        if len(split) == 6:
            mac = ':'.join(split).upper()

        split = line.replace('\t', '').split(': ')
        if len(split) == 2:
            if split[0] == 'SSID':
                ssid = split[1]
            if split[0] == 'signal':
                strength = split[1]

    return {
        'ssid': ssid,
        'strength': strength,
        'mac': mac
    }


def wifi_connections(*args, **kwargs):
    print('Received request for wifi option')
    result = subprocess.run(
        ['iwlist', 'wlan0', 'scan'], stdout=subprocess.PIPE)
    output = result.stdout.decode('utf-8').split('\n')
    parsed_cells = get_parsed_cells(output)

    _data = getSignalStrength()

    for p in parsed_cells:
        p['connected'] = p['Address'] == _data['mac']

    json_object = json.dumps(parsed_cells)

    r = requests.post(configuration.host + '/api/crosses/' + serial + '/wifi/available', data={
                      'data': json_object}, headers={'Authorization': configuration.passphrase, 'Accept': 'application/json'})


def connect_wifi(*args, **kwargs):
    global serial, reboot_pi, stop

    print('Received instruction to update wifi configuration')

    r = requests.get(configuration.host + '/api/crosses/' + serial + '/wifi/connect',
                     headers={'Authorization': configuration.passphrase, 'Accept': 'application/json'})
    data = r.json()

    fo = open("/etc/wpa_supplicant/wpa_supplicant.temp", "w+")
    # fo = open("wpa_supplicant.temp", "w+")
    fo.write("ctrl_interface=DIR=/run/wpa_supplicant GROUP=netdev\n")
    fo.write("update_config=1\n")
    for p in data:
        fo.write("network={\n")
        fo.write("\tssid=\"" + p['ssid'] + "\"\n")
        # fo.write("\tscan_ssid=" + p['scan_ssid'] + "\n")
        fo.write("\tscan_ssid=1\n")
        # fo.write("\tkey_mgmt=" + p['key_mgmt'] + "\n")
        fo.write("\tkey_mgmt=WPA-PSK\n")
        fo.write("\tpsk=\"" + p['psk'] + "\"\n")
        fo.write("}\n")
    fo.close()
    os.rename("/etc/wpa_supplicant/wpa_supplicant.temp",
              "/etc/wpa_supplicant/wpa_supplicant.conf")

    print('reboot instruction received after changing wifi configuration')
    reboot_pi = True
    stop = True


def beat():
    global serial, is_registered
    while True:
        try:
            if not is_registered:
                getSerial()
            _temp = getTemp()
            _data = getSignalStrength()
            print(_data['strength'])
            r = requests.post(configuration.host + '/api/crosses/beat', data={'serial': serial, 'version': version, 'temperature': _temp,
                              'strength': _data['strength']}, headers={'Authorization': configuration.passphrase, 'Accept': 'application/json'})
        except:
            print('Heartbeat failed')
        sleep(60)


serial = getSerial()
temp = getTemp()

print('Serial:', serial)
print("CPU temp: " + temp)

stop = False
force_exit = False
reboot_pi = False

last_api_time_calls = {}

# Define the minimum time interval (10 minutes)
interval = datetime.timedelta(minutes=10)

pusher = pysher.Pusher(configuration.key, configuration.cluster)


def download_collection(*args, **kwargs):
    print('Received update instruction')
    global stop
    global last_api_time_calls

    print('Getting updates from server', configuration.host +
          '/api/crosses/' + serial + '/instructions')
    r = requests.get(configuration.host + '/api/crosses/' + serial + '/instructions',
                     headers={'Authorization': configuration.passphrase, 'Accept': 'application/json'})
    print('Saving updates')
    with open('data.json', 'w') as outfile:
        json.dump(r.json(), outfile)
    print('Stopping shiftout loop')
    stop = True
    last_api_time_calls = {}


def get_current_temperature_scene(video_id):

    global last_api_time_calls

    current_time = datetime.datetime.now()

    last_api_time_call = last_api_time_calls.get(video_id, None)

    if last_api_time_call is None or (current_time - last_api_time_call) >= interval:

        last_api_time_calls[video_id] = current_time

        print(
            f'Getting current temperature scene for video ID {video_id} from server')
        r = requests.get(configuration.host + '/api/crosses/' + serial + '/videos/' + str(video_id) + '/temperature',
                         headers={'Authorization': configuration.passphrase, 'Accept': 'application/json'})

        if r.status_code == 200:
            return [True, r.json()['scene']]
        else:
            print(
                f'Error getting temperature scene for video ID {video_id} from server')
            return [False, []]

    print(f'Using cached temperature scene for video ID {video_id}')
    return [True, []]


def update(*args, **kwargs):
    global stop
    global force_exit
    print('update instruction received')
    g = git.cmd.Git()
    g.pull()
    print('Git pull - done')

    data = {}

    r = requests.post(configuration.host + '/api/crosses/data', data={'serial': serial, 'data': json.dumps(
        data)}, headers={'Authorization': configuration.passphrase, 'Accept': 'application/json'})

    print('Stopping shiftout loop')

    force_exit = True
    stop = True


def reboot_listener(*args, **kwargs):
    global reboot_pi
    global stop
    print('reboot instruction received')
    reboot_pi = True
    stop = True


data_pin = 15
latch_pin = 37
clock_pin = 13


def print_msg():
    print('Program is running...')
    print('Please press Ctrl+C to end the program...')


def destroy():
    GPIO.cleanup()


def setup():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)

    GPIO.setup(data_pin, GPIO.OUT)
    GPIO.setup(latch_pin, GPIO.OUT)
    GPIO.setup(clock_pin, GPIO.OUT)

    GPIO.output(data_pin, GPIO.LOW)
    GPIO.output(latch_pin, GPIO.LOW)
    GPIO.output(clock_pin, GPIO.LOW)


def dicimal_to_binary_array(decimal):
    # Remove the '0b' prefix and get the binary string
    binary_string = bin(decimal)[2:]

    # Ensure the binary string is 8 bits long by adding leading zeros if necessary
    while len(binary_string) < 8:
        binary_string = '0' + binary_string

    # Convert the binary string to an array of 1s and 0s
    binary_array = [int(bit) for bit in binary_string]

    return binary_array


def loop(_collections):
    global stop
    global force_exit

    try:
        for collection in _collections:

            if stop:
                print('Stopping loop - 1')
                return True

            for scene in collection:

                if stop:
                    print('Stopping loop - 2')
                    return True

                if scene['type'] == 'temperature':

                    [success, tempScene] = get_current_temperature_scene(
                        scene['video_id'])

                    scene['scene'] = tempScene if len(
                        tempScene) > 0 else scene['scene'] if success else []

                scene_iterations = scene['iterations']
                for l in range(scene_iterations):

                    if stop:
                        print('Stopping loop - 3')
                        return True

                    for x, frame in enumerate(scene['scene']):

                        if stop:
                            print('Stopping loop - 4')
                            return True

                        GPIO.output(latch_pin, False)

                        for bit in frame['bits']:
                            for b in dicimal_to_binary_array(bit):
                                GPIO.output(clock_pin, False)
                                GPIO.output(data_pin, True if int(
                                    b) == 1 else False)
                                GPIO.output(clock_pin, True)

                        GPIO.output(latch_pin, True)
                        if 'duration' in frame.keys():
                            sleep(frame['duration'] / 1000)
                        else:
                            sleep(frame['iterations'] / 600)

    except Exception as e:
        print('Error in loop', e)

    return stop


def connect_handler(data):
    channel = pusher.subscribe('cross-channel-'+str(serial))
    channel.bind('update_collection', download_collection)
    channel.bind('reboot', reboot_listener)
    channel.bind('update', update)
    channel.bind('scan_wifi', wifi_connections)
    channel.bind('connect_wifi', connect_wifi)

    channel_all = pusher.subscribe('cross-channel-all')
    channel_all.bind('reboot_all', reboot_listener)
    channel_all.bind('update_all', update)


pusher.connection.bind('pusher:connection_established', connect_handler)
pusher.connect()

print('Pusher Connected')


def reboot_now():
    os.system('sudo shutdown -r now')


if __name__ == '__main__':
    setup()

    x = threading.Thread(target=beat, daemon=True)
    x.start()
    print('Heartbeat started')

    try:
        while True:
            stop = False
            _break = False

            if reboot_pi:
                print('Rebooting...')
                reboot_now()
                sys.exit(0)

            if force_exit:
                print('Exiting...')
                sys.exit(0)

            print('Reading data.json')

            with open('data.json') as json_file:
                try:
                    collections = json.load(json_file)
                    print('data.json loaded')
                except:
                    print('Error reading data.json')
                    collections = []
            print('Starting loop')

            while not _break:
                _break = loop(collections)

    except KeyboardInterrupt:
        destroy()
